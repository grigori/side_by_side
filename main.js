let fs = require('fs');
let readline = require('readline');
function print_side_by_side(a, b, size=30,space=4){
    while(a || b){
        console.log(a.slice(0, size).padStart(size, " ") + " ".repeat(space) + b.slice(0,size))
        a = a.slice(size)
        b = b.slice(size)
    }
}

let cmd_args = process.argv.slice(2);

let readFile = function (file) {
    return new Promise(function (resolve, reject) {
        let lines = [];
        let rl    = readline.createInterface({
            input: fs.createReadStream(file)
        });

        rl.on('line', function (line) {
            lines.push(line);
        });

        rl.on('close', function () {
            lines = lines.join(" ");
            resolve(lines)
        });
    });
};
let lines1 = readFile(cmd_args[0]);
lines1.then(function(result1) {
    let lines2 = readFile(cmd_args[1]);
    lines2.then(function(result2) {
        print_side_by_side(result1, result2);
    })
})